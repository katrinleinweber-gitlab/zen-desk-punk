# Zendesk duplicate internal request info to CDot

## Purpose

A userscript for Zendesk tickets.

This script copies information from an Internal Request to the CDot `New License` form. \
Supported internal requests:
- `IR - SM - Extend an existing trial`
- `IR - SM - Extend an (almost) expired subscription`
- `IR - SM - Hacker One Reporter License`
- `IR - SM - Wider Community License`
- `IR - SM - NFR license request`
- `IR - SM - Problems starting a new trial`

Works in both CDot and Staging CDot.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js

## Use

Hotkeys:
- `ctrl-i-c` to copy info from the Zendesk IR ticket
- `ctrl-i-v` to paste info into CDot's `Licenses > Add new license` form

Alternatively use the commands from the userscript manager extension dropdown:
- `Zendesk duplicate internal request info to CDot > IR info - copy` 
- `Zendesk duplicate internal request info to CDot > IR info - paste`

License duplication is a two step manual process:
- Go to the Zendesk IR ticket to copy information
- Go to the `Licenses > Add new license` form in CDot to paste the information

The script copies any `true-ups` entered in the IR, but doesn't fill in PUC. \
You will have to do the unholy math yourself.

Always double check the pasted information!

## Support Ops - Team Member License

`Team member license` request handling differs in a few ways:
- Requests are not Zendesk tickets but GitLab issues
- Licenses are created in Staging CDot
- After creating the license, the clipboard will contain a reply which you can paste as closing comment in the request

## Changelog

- 3.10 New team license reply snippet
- 3.9 Greedy CDot URL matching
- 3.8 Handle 'IR - ' title change
- 3.7 Remove obsolete VM-UI
- 3.6 Update VM-UI to 0.7.8
- 3.5 Implemented Support Ops Team Member License
- 3.4 Simplify setPlan
- 3.3 Add delay in catch
- 3.2 More safeguards for setPlan, plus logging
- 3.1 Add try/catch around setPlan
- 3.0 Retry plan dropdown selection if needed
- 2.9 Give dropdown lists a bit more time
- 2.8 Give dropdown lists a bit more time
- 2.7 Fix for Agent Workspace
- 2.6 Reliable dropdown list selection
- 2.5 Make true-ops warning last action
- 2.4 Fixed flaky paste
- 2.3 Add special license indication to Notes
- 2.2 Support `IR - SM - Problems starting a new trial`
- 2.1 Properly detect active tab ticket
- 2.0 Scrape info from active tab if multiple tickets are opened
- 1.9 Properly detect active tab ticket type 
- 1.8 Make `Expires at` entered value accepted (for real this time)
- 1.7 Proper true-ups warning 
- 1.6 Match Staging CDot
- 1.5 Warning when true-ups are filled in
- 1.4 Make `Expires at` entered value accepted
- 1.3 RailsAdmin update requires modified `Expires at` field handling 
- 1.2 Set `License type` to `Legacy`
- 1.1 Only scrape email from `Contact's email` field
- 1.0 Public release
