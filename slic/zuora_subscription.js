'use strict';

function scrapeZuoraSubscription(url) {
  let info = {};
  info.url = url;

  info.name = document.querySelector('div.header-text div h5').textContent;

  const status_active = document.querySelector('div.badge-wrapper button.active span.badge-text');
  const status_expired = document.querySelector('div.badge-wrapper button.expired');
  const status_cancelled = document.querySelector('div.badge-wrapper button.cancelled');
  if (status_active) {
    info.status = status_active.textContent;
  } else if (status_expired) {
    info.status = status_expired.textContent;
  } else if (status_cancelled) {
    info.status = status_cancelled.textContent;
  } else {
    info.status = '??? unknown';
  }

  if (status_active || status_cancelled) {
    info.version = document.querySelector('div.badge-wrapper button.version-badge').textContent;
  } else if (status_expired) {
    info.version = document.querySelector('div.badge-wrapper button.version-badge-expired').textContent;
  } else {
    info.version = '??? unknown'
  }

    info.customer_account = document.querySelector('span.customer_account').nextSibling.textContent;
  // Note to future me:
  // In case scraping starts to fail, check if Zuora fixed their 'inovice_owner' typo
  info.invoice_owner = document.querySelector('span.inovice_owner').nextSibling.textContent;

  info.subscription_start = document.querySelector('span.subscription_start_date').nextSibling.textContent;
  info.subscription_end = document.querySelector('span.subscription_end_date').nextSibling.textContent;

  info.term = document.querySelector('span.current_term').nextSibling.textContent;
  info.term_start = document.querySelector('span.term_start_date').nextSibling.textContent;
  info.term_end = document.querySelector('span.term_end_date').nextSibling.textContent;

  info.cloud_licensing = document.querySelector('span.turn.on.cloud.licensing').nextSibling.textContent;

  info.products = [];
  [...document.querySelectorAll('tr.charge-row')].forEach( row => {
    const cells = row.querySelectorAll('td');
    let product = {};
    product.description = cells[2].textContent.trim();
    product.quantity = cells[7].textContent.trim();
    product.period = cells[10].textContent.trim();
    info.products.push(product);
  });

  return pasteZuoraSubscription(info);
}

function pasteZuoraSubscription(info) {
  let text = `Zuora Subscription information:\n` +
    `URL = ${info.url}\n` +
    `Name = ${info.name}\n` +
    `Status = ${info.status}\n` +
    `Version = ${info.version}\n` +
    `Customer account = ${info.customer_account}\n` +
    `Invoice owner = ${info.invoice_owner}\n` +
    `Subscription start = ${info.subscription_start}\n` +
    `Subscription end = ${info.subscription_end}\n` +
    `Term = ${info.term}\n` +
    `Term start = ${info.term_start}\n` +
    `Term end = ${info.term_end}\n` +
    `Cloud Licensing = ${info.cloud_licensing}\n`;
  text += 'Products:\n';
  const products = info.products.sort( (a, b) => { return a.period.localeCompare(b.period)});
  products.forEach( prod => {
    text += `- Period:      ${prod.period}\n`;
    text += `  Description: ${prod.description}\n`;
    text += `  Quantity:    ${prod.quantity}\n`;
  });
  return text;
}
