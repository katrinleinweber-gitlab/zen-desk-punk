// ==UserScript==
// @name          Zendesk Window Cleaner
// @version       1.1
// @author        Tom McAtee
// @description   Zendesk: minimises the left, right, and bottom composer panes
// @match         https://gitlab.zendesk.com/agent/*
// @license       MIT
// @grant    GM.registerMenuCommand
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_window_cleaner
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_window_cleaner/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_window_cleaner/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_window_cleaner/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==
'use strict';


(function() {
    'use strict';

    function toggleLeftHandPane(desiredToggleState){
        let check = isLeftHandPaneVisible();

        if(check == desiredToggleState){
            return;
        }

        let leftPane_toggle = document.querySelectorAll("[data-test-id='ticket-fields-splitter-button']");
        if(leftPane_toggle.length == 1){
            leftPane_toggle[0].click();
        }
    }

    function toggleRightHandPane(desiredToggleState){
        let check = isRightHandPaneVisible();

        if(check == desiredToggleState){
            return;
        }

        let rightPane_toggle = document.querySelectorAll("[aria-pressed='true']");
        if(rightPane_toggle.length == 1) {
            rightPane_toggle[0].click();
        }
    }

    function isNullOrUndefined(item) {
            return item == undefined || item == null;
    }

    function isRightHandPaneVisible() {
       let leftHandSplitter = document.querySelector("[data-test-id='ticket-context-panel-splitter-wrapper']");
       if(isNullOrUndefined(leftHandSplitter)) {
           return false;
       } else {
           return true;
       }
    }

    function isLeftHandPaneVisible() {
       let leftHandSplitter = document.querySelector("[data-test-id='ticket-fields-splitter-wrapper']");

       if(isNullOrUndefined(leftHandSplitter)) {
           return false;
       } else {
           return true;
       }
    }

    function isComposerPaneVisible() {
        let isComposerVisible = document.querySelector("[label='Hide composer']")

        if(isNullOrUndefined(isComposerVisible)) {
            return false;
        }
        return true;
    }

    function toggleComposePane(desiredToggleState){
        let composerIsVisible = isComposerPaneVisible();

        if(composerIsVisible == desiredToggleState) {
            return;
        }

        let compose_toggle = document.querySelectorAll("[data-garden-id='pane.splitter_button']");
        if(compose_toggle.length == 1){
            compose_toggle[0].click();
        }
    }

    function minimisePanes() {
        let makePanesVisible = !isComposerPaneVisible();
        toggleComposePane(makePanesVisible);
        toggleRightHandPane(makePanesVisible);
        toggleLeftHandPane(makePanesVisible);
        //TODO: Handle the case where the right hand pane is already minimised
    }

    function checkIfPageIsTicket() {
        const currentUrl = window.location.href;
        // Check if the current URL contains the substring 'tickets' to avoid running on non-ticket pages
        if(currentUrl.indexOf("tickets")!=-1 ) {
            minimisePanes();
        }
    }

    // Add handling for triggering the script
    GM.registerMenuCommand("Clear View", () => {
        checkIfPageIsTicket();
    });

    function hotKeyHandler(event) {
        if(event.ctrlKey && event.shiftKey && event.code == "KeyA") {
            checkIfPageIsTicket();
        }
    }

    window.addEventListener("keydown", hotKeyHandler, true);
})();