// ==UserScript==
// @name          Zendesk Squeeze Region & Type
// @version       1.6
// @author        Rene Verschoor & Katrin Leinweber
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/katrinleinweber/zendesk-punk
// @homepageURL   https://gitlab.com/katrinleinweber/zendesk-punk/-/tree/main/zendesk_squeeze_region_type
// @downloadURL   https://gitlab.com/katrinleinweber/zendesk-punk/-/raw/main/zendesk_squeeze_region_type/script.user.js
// @installURL    https://gitlab.com/katrinleinweber/zendesk-punk/-/raw/main/zendesk_squeeze_region_type/script.user.js
// @updateURL     https://gitlab.com/katrinleinweber/zendesk-punk/-/raw/main/zendesk_squeeze_region_type/script.user.js
// @supportURL    https://gitlab.com/katrinleinweber/zendesk-punk/issues/new
// ==/UserScript==

'use strict';

const MINIMUM_NR_COLUMS = 4;
const SELECTOR_HEADER = '[data-test-id="generic-table"] > thead > tr > th';
const SELECTOR_CELLS = '[data-garden-id="tables.cell"]';
const MAGIC_WORDS = {
                      'Due date': '⏰'        ,
                      'Priority': 'Prio'      ,
                      'Assignee': '🧑‍💻'        ,
                     'Requester': '🗣️'        ,
                  'Organization': '🏢'        ,
                   'Emergencies': '🚨🚨🚨'    ,
                        'Urgent': '🚨🚨'      ,
                          'High': '🚨'        ,
                        'Normal': ''          ,
                           'Low': ''          ,
                           'L&R': ''          ,
  'Preferred Region for Support': '🗺️',
  'L&R Product Type': 'Type',
  'Ticket form': '📑',
  'Alliance Partners' : '🤝',
  'All Regions': '🌎🌍🌏',
  'Americas, USA - 05:00 to 17:00 PT (US & Canada)': '',
  'Asia Pacific - 23:00 to 11:00 UTC': '',
  'Europe, Middle East, Africa - 08:00 to 18:00 CET': '🌍',
  'GitLab Incidents': '🚨🚨',
  'GitLab Dedicated': '☁️',
  'SaaS': '☁️☁️',
  'Self-Managed': '',
  'SaaS Account': '',
  'Support Internal Request': 'IR'
};

  (function() {

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver( mutations => {
    po(mutations);
  });
  observer.observe(document.body, { childList: true, subtree: true });

  function po(mutations) {
    let ths = document.querySelectorAll(SELECTOR_HEADER);
    if (ths.length >= MINIMUM_NR_COLUMS) {
      ths.forEach(th => {
        if (th.textContent in MAGIC_WORDS)
          th.textContent = MAGIC_WORDS[th.textContent];
      })
    }
    mutations.forEach( mutation => {
      mutation.target.querySelectorAll(SELECTOR_CELLS).forEach( element => {
        if (element.textContent in MAGIC_WORDS)
          element.textContent = MAGIC_WORDS[element.textContent];
      })
    })
  }

})();
