# Zendesk Squeeze Region & Type

## Purpose

A userscript for Zendesk views. \
It makes tables a bit more compact.

This script changes:
- `Preferred Region for Support` column
  - Change header to`Region`
  - Change cells:
    - `All Regions` -> `All`
    - `Europe, Middle East, Africa` -> `EMEA`
    - `Americas, USA` -> `AMER`
    - `Asia Pacific` -> `APAC`
- `L&R Product Type` column
  - Change header to `Type`
  - Change cells:
    - `GitLab Dedicated` -> `Dedicated`
    - `GitLab.com` -> `.com`
    - `Self-Managed` -> `SM`
- `Ticket form` column
  - Change header to `Form`
    - `Self-Managed` -> `SM`
    - `GitLab.com` -> `.com`
    - `SaaS` -> `.com`
    - `GitLab Dedicated` -> `Dedicated`
    - `Alliance Partners` -> `Partner`
    - `Support Internal Request` -> `IR`
    - `SaaS Account` -> `Account`
    - `GitLab Incidents` -> `Incidents`

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_squeeze_region_type/script.user.js

## Warning

The script uses a very blunt approach to update the cell content. \
It just matches on exact texts.
Let me know if you find a way to target only the correct column!

Due to how Zendesk updates the tables, this script is not perfect. \
When you switch between Zendesk views you'll see that an incorrect column will have the `OOO` title.
If this bothers you, a page refresh will solve it.

Despite this deficiency I find the script very useful. 

## Technical

## Changelog

- 1.5 Add 'GitLab.com' again, in use by L&R
- 1.4 Added Incidents, rename SaaS
- 1.3 Squeeze 'SaaS Account' and 'Support Internal Request'
- 1.2 Squeeze 'GitLab Dedicated'
- 1.1 SaaS rename werkverschaffingsprojekt
- 1.0 Public release
