// ==UserScript==
// @name          ZenDesk trim needs org reminder
// @version       1.0.0
// @author        Katrin Leinweber
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_needs_org
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_needs_org/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_needs_org/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_needs_org/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/
// ==/UserScript==

'use strict';

const DEBUG = false;
const HEAD = "THIS IS A NEEDS ORG TICKET";
const L1 = "Please note you should not reply";
const L2 = "Once associated, the";

(function () {

  // Watch for any change on the Zendesk page
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    po();
    rm_header();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  // Find reminder content
  function po() {
    let selector = '[id^=ember] > div.comment > div > p';
    let elements = document.querySelectorAll(selector);
    elements.forEach((element, index) => {
      let text = element.textContent
      if (text.startsWith(L1) || text.startsWith(L2)) {
        go(elements, index);
      }
    })
  }

  function rm_header() {
    let selector = '[id^=ember] > div.comment > div > h2';
    let elements = document.querySelectorAll(selector);
    elements.forEach((element, index) => {
      if (element.textContent === HEAD) {
        go(elements, index);
      }
    })
  }

  // Remove elements
  function go(elements, index) {
    if (DEBUG) {
      elements[index].style.backgroundColor = 'hotpink';
    } else {
      elements[index].style.display = 'none';
    }
  }

})();
