// ==UserScript==
// @name          Zendesk Hi
// @version       1.4
// @author        Rene Verschoor
// @description   Zendesk: Say Hi
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hi
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hi/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hi/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hi/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function () {

  const HOTKEY = [
    {"shift": false, "control": true, "alt": false, "meta": false, "key": "h"},
    {"shift": false, "control": true, "alt": false, "meta": false, "key": "i"}
  ]

  const hotkeyListener = (sequence, callback) => {
    let index = 0;
    let timerId;
    const TIMEOUT = 500;

    return (e) => {
      const step = sequence[index];
      if (e.key === step.key &&
        step.shift === e.shiftKey &&
        step.control === e.ctrlKey &&
        step.alt === e.altKey &&
        step.meta === e.metaKey) {
        if (index === sequence.length - 1) {
          callback();
        }
        clearTimeout(timerId);
        if (index !== sequence.length - 1) {
          timerId = setTimeout(() => index = 0, TIMEOUT);
        }
        index = (index + 1) % sequence.length;
      } else {
        index = 0;
        clearTimeout(timerId);
      }
    };
  };

  const eventListener = hotkeyListener(HOTKEY, () => {
    sayHi();
  });
  document.addEventListener('keyup', eventListener, true);

  function sayHi() {
    const ticket = getActiveTicket();
    const name = getName(ticket);
    pasteName(ticket, name);
  }

  function pasteName(ticket, name) {
    const editor = ticket.querySelector('.ck-editor__editable').ckeditorInstance;
    editor.focus();
    editor.model.change( writer => {
      editor.model.insertContent(writer.createText(greeting(name)));
    });
  }

  function greeting(name) {
    return (`Hi ${name},`);
  }

  function getName(ticket) {
    let name;
    if (isInternalRequest()) {
      name = internalRequestName(ticket);
    } else {
      name = normalTicketName(ticket);
    }
    return name.split(" ")[0];
  }

  function internalRequestName(ticket) {
    const requester = ticket.querySelector('[data-test-id="ticket-fields-collaborators"] span');
    return requester.textContent;
  }

  function normalTicketName(ticket) {
    const requester = ticket.querySelector('[data-test-id="ticket-system-field-requester-select"] div');
    return requester.textContent;
  }

  function isInternalRequest() {
    const title = getTicketTitle();
    return (title.startsWith('IR - ') || title.startsWith('Internal Request - '));
  }

  function getActiveTicket() {
    let workspaces = document.querySelectorAll('.workspace');
    let activeWorkspace;
    for (let workspace of workspaces) {
      if (workspace.style['visibility'] !== 'hidden' && workspace.style['display'] !== 'none') {
        activeWorkspace = workspace;
        break;
      }
    }
    return activeWorkspace;
  }

  function getTicketTitle() {
    return document.querySelector('div[role=tab][data-selected=true] [data-test-id="header-tab-title"]').textContent;
  }

})();
