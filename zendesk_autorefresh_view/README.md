# Zendesk autorefresh view

## Purpose

Some people need or want to be on top of the queue. \
Zendesk doesn't automatically refresh the views, so you have to manually refresh to see if
new tickets arrived. \
This userscript will try to do the refreshing for you.

**Q**: My browser has built-in functionality to reload a tab, so I don't need this script.\
**A**: It will also reload while you're editing a ticket. The userscript will not do that.

**Q**: It doesn't always refresh at every interval. \
**A**: Browsers can put a tab to sleep if it doesn't have focus. The script will
then not run, and no refresh happens.

**Q**: Isn't refreshing every minute too much for Zendesk? \
**A**: Zendesk recommends using a
[3rd party browser extension](https://support.zendesk.com/hc/en-us/articles/4408827334042-Can-views-be-refreshed-automatically)
for automatic refreshes. \
The 2 extensions it links to show configurable intervals of seconds, 
so I think my 1 minute is acceptable.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_autorefresh_view/script.user.js

## Configuration

The default refresh interval is 1 minute.\
You can configure the refresh interval in the userscript settings.

## Changelog

1.1 - Improved clicking
1.0 - First public release