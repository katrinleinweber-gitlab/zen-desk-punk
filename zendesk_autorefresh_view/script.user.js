// ==UserScript==
// @name          Zendesk autorefresh view
// @version       1.1
// @author        Rene Verschoor
// @description   Zendesk: Autorefresh view
// @match         https://gitlab.zendesk.com/*
// @require       https://openuserjs.org/src/libs/sizzle/GM_config.js
// @grant         GM.registerMenuCommand
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_autorefresh_view
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_autorefresh_view/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_autorefresh_view/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_autorefresh_view/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {

  let lastUrl = null;
  let matchUrl = null;
  let intervalTimer = null;
  let interval = null;

  function clog(txt, color = 'aqua') {
    console.log('%c' + txt, `color: ${color}`);
  }

  window.addEventListener("focus", gotFocus);
  function gotFocus() {
    // clog('Got focus');
    refreshView();
  }

  new MutationObserver(() => {
    if (location.href !== lastUrl) {
      lastUrl = location.href;
      urlChange(location.href);
    }
  }).observe(document, {subtree: true, childList: true})

  function urlChange(new_url) {
    if (new_url.startsWith('https://gitlab.zendesk.com/agent/filters/')) {
      // clog('Autorefresh: URL match');
      matchUrl = true;
      startRefreshTimer();
    } else {
      // clog('Autorefresh: URL mismatch');
      matchUrl = false;
      cancelRefreshTimer();
    }
  }

  function startRefreshTimer() {
    if (interval !== null && matchUrl) {
      // clog(`Autorefresh: start, interval = ${interval}m`);
      cancelRefreshTimer();
      intervalTimer = setInterval(refreshView, interval * 60 * 1000);
    }
  }

  function cancelRefreshTimer() {
    clearInterval(intervalTimer);
  }

  function refreshView() {
    // clog(`Autorefresh: refresh page`);
    let button = document.querySelector('[data-test-id="views_views-list_header-refresh"]');
    button.click();
  }

  //---------------------------------------------
  // Configuration
  //---------------------------------------------
  const iframecss = `
    height: 440px;
    width: 350px;
    border: 1px solid;
    border-radius: 3px;
    position: fixed;
    z-index: 9999;
  `;

  const css = `
    #RefreshConfig {
      background: azure;
    }
    .config_var {
      padding-bottom: 10px;
    }
    #RefreshConfig .reset_holder {
      float: right;
      position: relative;
      padding-right: 10px;
    }
  `;

  let gmc = defineConfig();

  GM.registerMenuCommand("Configure", () => {
    configure();
  });

  function configure() {
    gmc.open();
    gmc.frame.style = iframecss;
  }

  function onSave() {
    readConfiguredInterval();
    location.reload();
  }

  function defineConfig() {
    return new GM_config(
      {
        'id': 'RefreshConfig',
        'title': 'Configure',
        'fields': {
          'interval':
            {
              'label': 'Refresh interval (minutes)',
              'type': 'int',
              'min': 1,
              'default': 1
            },
        },
        'events': {
          'init': onConfigInit,
          'save': onSave
        },
        'css': css
      });
  }

  function onConfigInit() {
    readConfiguredInterval();
  }

  function readConfiguredInterval() {
    interval = gmc.get('interval');
    startRefreshTimer();
  }

})();

