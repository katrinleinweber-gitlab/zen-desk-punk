# Zendesk Hide Unread Message

## Purpose

Zendesk regularly shows an indication that there are unread messages, except there aren't.\
While waiting for a bugfix from Zendesk, just DIY and hide the often useless indicator.\

![](img/thetabislying.png) 
![](img/thebuttonislying.png)

# Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_unread_message/script.user.js

## Changelog

- 1.0 Initial release
