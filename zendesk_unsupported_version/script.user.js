// ==UserScript==
// @name          Zendesk unsupported version
// @version       1.7
// @author        Rene Verschoor
// @description   Zendesk: unsupported GitLab version warning
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_unsupported_version
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const SUPPORTED_VERSIONS = ['15', '16', '17'];
const HANDBOOK_URL = 'https://about.gitlab.com/support/statement-of-support/#version-support';

const VULNERABLE_VERSIONS = [
  '16.1', '16.1.0', '16.1.1', '16.1.2', '16.1.3', '16.1.4', '16.1.5',
  '16.2', '16.2.0', '16.2.1', '16.2.2', '16.2.3', '16.2.4', '16.2.5', '16.2.6', '16.2.7', '16.2.8',
  '16.3', '16.3.0', '16.3.1', '16.3.2', '16.3.3', '16.3.4', '16.3.5', '16.3.6',
  '16.4', '16.4.0', '16.4.1', '16.4.2', '16.4.3', '16.4.4',
  '16.5', '16.5.0', '16.5.1', '16.5.2', '16.5.3', '16.5.4', '16.5.5',
  '16.6', '16.6.0', '16.6.1', '16.6.2', '16.6.3',
  '16.7', '16.7.0', '16.7.1'
];

(function() {

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    pogo();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  function pogo() {
    let ticket= getActiveTicket();
    if (ticket) {
      let selector = '.custom_field_43970347 input';
      let elements = ticket.querySelectorAll(selector);
      elements.forEach((element, index) => {
        if (isUnsupported(element.value)) {
          highlightVersionField(element);
          linkifyVersionField(element);
        }
        highlightHeader(ticket, isVulnerable(element.value));
      })
      if (elements.length === 0) {
        highlightHeader(ticket, false);
      }
    }
  }

  function getActiveTicket() {
    let workspaces = document.querySelectorAll('.workspace');
    let activeWorkspace;
    for (let workspace of workspaces) {
      if (workspace.style['visibility'] !== 'hidden' && workspace.style['display'] !== 'none') {
        activeWorkspace = workspace;
        break;
      }
    }
    return activeWorkspace;
  }

  function isUnsupported(version) {
    let major = version.split('.')[0];
    return major.length && !SUPPORTED_VERSIONS.includes(major);
  }

  function highlightVersionField(field) {
    field.style.boxShadow = '0px 0px 10px 5px red';
  }

  function linkifyVersionField(field) {
    const punked = field.getAttribute('punked');
    if (!punked) {
      field.setAttribute('punked', 'true');
      const labelElement = field.previousElementSibling;
      const newElement = Object.assign(document.createElement('a'), {
        href: HANDBOOK_URL,
        target: '_blank',
        textContent: labelElement.textContent
      });
      labelElement.textContent = '';
      labelElement.appendChild(newElement);
    }
  }

  function isVulnerable(version) {
    let vulnerable = false;
    if (version !== null && version !== undefined && version.length > 0) {
      vulnerable = VULNERABLE_VERSIONS.includes(version);
    }
    return vulnerable;
  }

  function highlightHeader(ticket, highlight) {
    const element = ticket.querySelector('div.workspace > header');
    element.style.boxShadow = highlight ? '10px 10px 10px red' : '';
  }

})();
