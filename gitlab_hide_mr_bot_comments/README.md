# GitLab - Hide MR bot comments

## Purpose

A userscript for GitLab merge requests

This script toggles the visibility of MR comments of the following bots:
- `Danger bot`
- `CI scripts API usage`
- `gitlab-org/database-team/gitlab-com-database-testing`

This compacts the MR, making it a bit more readable, and have you scroll less.

Toggle the display of the bot comments with the hotkey `Control-Option-b` (mac) or `Control-Alt-b`.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/gitlab_hide_mr_bot_comments/script.user.js

## Changelog

- 1.2 Add 'DANGER_GITLAB_API_TOKEN'
- 1.1 Add 'TEST_SLOW_NOTE_PROJECT_TOKEN'
- 1.0 Public release
