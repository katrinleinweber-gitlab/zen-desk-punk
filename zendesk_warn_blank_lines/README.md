# Zendesk Warn Blank Lines

## Purpose

A userscript for Zendesk tickets.

This script highlights the Submit button with a red warning box if you have blank lines at the end of your message helping prevent blank space between your reply and signature.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_warn_blank_lines/script.user.js

## Changelog

- 1.0 Public release
